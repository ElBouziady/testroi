#include "mainwindow.h"
#include <QApplication>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>
#include <stdio.h>
#include <QFile>
#include <QTextStream>

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    Mat src,raw;

    QStringList list;
    QFile inputFile("/home/boukary/LocalDataset/hopital/negative2.txt");
    int j = 912;
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            list=line.split(" ");
            src=imread(list.at(0).toStdString().c_str());
            raw=imread(list.at(0).toStdString().c_str());
            cout<<list.at(0).toStdString().c_str()<<endl;
            if(list.at(1).toInt()==0)
                continue;
            for(int i=2;i<list.size();i+=4)
            {

                rectangle(src,Rect(list.at(i).toInt(),list.at(i+1).toInt(),list.at(i+2).toInt(),list.at(i+3).toInt()),Scalar(0,255,0),2);
                imwrite(format("/home/boukary/LocalDataset/hopital/negatives/bg%05d.bmp",j++),raw(Rect(list.at(i).toInt(),list.at(i+1).toInt(),list.at(i+2).toInt(),list.at(i+3).toInt())));

            }
            putText(src,list.at(0).toStdString().c_str(),cvPoint(24,24),FONT_HERSHEY_COMPLEX_SMALL,0.8,cvScalar((0,0,255),2, CV_AA));

            imshow("image",src);
            waitKey(0);
        }
        inputFile.close();
    }


    return a.exec();
}
